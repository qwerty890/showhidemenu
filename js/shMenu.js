$(document).ready(function() {

  var menu = $('.shNav');
  var ul = menu.find('ul');
  var li = menu.find('li');

  var optionsBox = $('.shNavOptions');
  // var hasChildClass = optionsBox.find('.hasChildClass').text();
  // var hasntChildClass = optionsBox.find('.hasntChildClass').text();

  var hasChildClass = "elementHasChildren";
  var hasntChildClass = "elementHasntChildren";
  var showOnHover = "false";
  var autoHideAfterClick = "true";
  var millisecondsToHideMenu = "1000";

  if(optionsBox.find('.hasChildClass').text() !== "") {
      hasChildClass = optionsBox.find('.hasChildClass').text();
  }

  if(optionsBox.find('.hasntChildClass').text() !== "") {
      hasntChildClass = optionsBox.find('.hasntChildClass').text();
  }

  if(optionsBox.find('.showOnHover').text() !== "") {
    showOnHover = optionsBox.find('.showOnHover').text();
  }

  if(optionsBox.find('.autoHideAfterClick').text() !== "") {
    autoHideAfterClick = optionsBox.find('.autoHideAfterClick').text();
  }

  if (optionsBox.find('.millisecondsToHideMenuItem').text() !== "") {
      millisecondsToHideMenu = optionsBox.find('.millisecondsToHideMenuItem').text();
  }

  li.children().has('li').hide();

  li.addClass(hasntChildClass);
  li.has('ul').removeClass(hasntChildClass).addClass(hasChildClass);

  $( menu ).on( "click", function( event ) {
    var target = event.target;

    if ( ! $(target).is('ul') ) {
      $(target).children().toggle();
      if ( autoHideAfterClick == 'true' ) {
        $(target).siblings().children().hide();
        $(target).siblings().children().find('ul').hide();
      }

      var linkToOpen = $(target).attr('link');
      if (linkToOpen != null) window.location = linkToOpen;
    }
  });

  var lastElementHover = "";

  $( menu ).hover( function( event ) {

    $( li ).hover( function( event ) {
      if (showOnHover == 'true') {
        var target = event.target;
        $(target).children().show();
        $(target).siblings().children().hide();
        lastElementHover = event.target;
      }

    });

  }, function( event ) {

    if (showOnHover == 'true') {
      var target = event.target;
      setTimeout(function() {
        if ($(menu).find("li:hover").length <= 0) {
          $(li).children().hide();
        }
      }, millisecondsToHideMenu);

      $(lastElementHover).children().show();

    }

  });



});
